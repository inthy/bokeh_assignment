import numpy as np
import pandas as pd
import datetime

df = None


def setup_df():
    global df
    df = pd.read_csv('data_itc.csv')
    df.dropna(1, inplace=True)
    df.date = pd.to_datetime(df.date)


def split_df():
    global df
    msk = df.date.max() - datetime.timedelta(days=30) < df.date
    train = df[~msk]
    test = df[msk]
    return train, test


setup_df()
print(df.date[0])